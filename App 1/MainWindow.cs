﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using App_1.Properties;

namespace App_1
{
    public partial class MainWindow : Form
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void MainWindow_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            button1.Text = Resources.MainWindow_button1_Click_TEXT;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            button2.Text = Resources.MainWindow_button2_Click_TEXT;
        }
    }
}
