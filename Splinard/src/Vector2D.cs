﻿namespace Splinard
{
    /// <summary>
    /// Represents a 2-dimensional value.
    /// </summary>
    public struct Vector2D {
        private double _x;
        private double _y;

        /// <summary>
        /// Creates a new Vector2 with predefined values.
        /// </summary>
        public Vector2D(double x, double y) {
            _x = x;
            _y = y;
        }

        public double X {
            get { return _x; }
            set { _x = value; }
        }

        public double Y {
            get { return _y; }
            set { _y = value; }
        }
    }
}
